﻿using System.Collections.Generic;
using System.Linq;
using _.Scripts;
using TMPro;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{

    [SerializeField]
    private GameObject spawnable;

    [SerializeField]
    private TextMeshProUGUI numberSpawned;

    protected int SpawnedMax = 2;
    protected float SpawnDistanceMax = 20;
    protected float SpawnFrequency = 2;
    private List<GameObject> _spawned = new List<GameObject>();
    protected string AnimalName = "Unnamed";


    // ENCAPSULATION
    private int _numberSpawned;
    private int NumberSpawned {
        get => _numberSpawned;
        set{
            _numberSpawned = value;
            numberSpawned.text = $"{value} {AnimalName} spawned!";
        }
    }


    void Start()
    {
        Invoke("SpawnLoop", SpawnFrequency); // ABSTRACTION
    }

    void Update()
    {
        DestroySurplus(); // ABSTRACTION
    }


    void SpawnLoop()
    {
        if (_spawned.Count < SpawnedMax)
            Spawn(); // ABSTRACTION

        Invoke("SpawnLoop", SpawnFrequency);
    }


    public void Spawn() // POLYMORPHISM
    {
        Spawn(1);
    }

    public void Spawn(int count) // POLYMORPHISM
    {
        for (int i = 0; i < count; i++)
        {
            Vector3 newPosition3 = Vector3.zero;
            Vector2 newPosition2 = SpawnDistanceMax * Random.insideUnitCircle;
            newPosition3 = transform.position + new Vector3(newPosition2.x, 0, newPosition2.y);
            GameObject newSpawn = Instantiate(spawnable, newPosition3, Quaternion.identity);
            newSpawn.transform.SetParent(transform);
            newSpawn.GetComponent<SpawnChildController>().parentSpawner = this;
            _spawned.Add(newSpawn);
            NumberSpawned++;
        }
    }


    public void DestroySpawn() // POLYMORPHISM
    {
        if (_spawned.Count > 0) DestroySpawn(_spawned[0]);
    }

    public void DestroySpawn(GameObject spawn) // POLYMORPHISM
    {
        for (int i = _spawned.Count - 1; i >= 0; i--)
            if (_spawned[i] == null || _spawned[i] == spawn)
                _spawned.RemoveAt(i);

        Destroy(spawn);
    }

    void DestroySurplus()
    {
        while (_spawned.Count > SpawnedMax) DestroySpawn();
    }
}
