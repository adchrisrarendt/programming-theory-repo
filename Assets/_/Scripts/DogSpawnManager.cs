﻿// INHERITANCE
public class DogSpawnManager : SpawnManager
{
    public DogSpawnManager()
    {
        SpawnedMax = 20;
        SpawnDistanceMax = 20;
        SpawnFrequency = .75f;
        AnimalName = "Dog";
    }
}
