﻿using System;
using UnityEngine;

public class InGameManager : MonoBehaviour
{
    public SpawnManager dogSpawner;
    public SpawnManager elephantSpawner;

    private void Start()
    {
        Debug.Log($"Chose {DataManager.Instance.animalType}");

        if (DataManager.Instance.animalType == "Dog") dogSpawner.gameObject.SetActive(true);
        else elephantSpawner.gameObject.SetActive(true);
    }
}
