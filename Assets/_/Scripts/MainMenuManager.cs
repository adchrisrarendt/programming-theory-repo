using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    public void StartGame(string animalType)
    {
        DataManager.Instance.animalType = animalType;
        SceneManager.LoadScene(1);
    }
}
