﻿// INHERITANCE
public class ElephantSpawnManager : SpawnManager
{

    public ElephantSpawnManager()
    {
        SpawnedMax = 6;
        SpawnDistanceMax = 20;
        SpawnFrequency = 2;
        AnimalName = "Elephant";
    }
}
