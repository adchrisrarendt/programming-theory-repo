﻿using System;
using System.IO;
using UnityEngine;

public class DataManager : MonoBehaviour
{
    public static DataManager Instance;
    public string animalType;

    private string _savePath;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);

        _savePath = Application.persistentDataPath + "/savefile.json";
        Load();
    }


    [Serializable]
    public class SaveData
    {
        public string animalType;
    }

    public void Load()
    {
        if (File.Exists(_savePath))
        {
            string jsonData = File.ReadAllText(_savePath);
            SaveData data = JsonUtility.FromJson<SaveData>(jsonData);

            animalType = data.animalType;
        }
    }

    public void Save()
    {
        SaveData data = new SaveData();
        data.animalType = animalType;

        string json = JsonUtility.ToJson(data);
        File.WriteAllText(_savePath, json);
    }
}
